#!/bin/ash

#Se espera a que se inicie MySQL:
echo "Waiting for MySQL..."
while [ "$(mysqladmin ping --host=$MYSQL_DB_HOST --user=$MYSQL_DB_USER --password=$MYSQL_DB_PASSWORD --port=$MYSQL_DB_PORT)" != "mysqld is alive" ]; do
    sleep 3
done
echo "MySQL started"

#Cambia las variables, para crear la DB:
sed -i "s/{MYSQL_DB_DATABASE}/$MYSQL_DB_DATABASE/g" /tmp/db_create.sql
sed -i "s/{MYSQL_DB_CHARSET}/$MYSQL_DB_CHARSET/g" /tmp/db_create.sql
sed -i "s/{MYSQL_DB_COLLATION}/$MYSQL_DB_COLLATION/g" /tmp/db_create.sql

#Crea la DB:
mysql --host=$MYSQL_DB_HOST --user=$MYSQL_DB_USER --password=$MYSQL_DB_PASSWORD --port=$MYSQL_DB_PORT < /tmp/db_create.sql

#Hace el restore de la DB.
mysql --host=$MYSQL_DB_HOST --user=$MYSQL_DB_USER --password=$MYSQL_DB_PASSWORD --port=$MYSQL_DB_PORT $MYSQL_DB_DATABASE < /tmp/bugt_restore.sql

cd /root/liquibase \
&& java -jar liquibase.jar \
--classpath="/root/liquibase/mysql-connector-java-5.1.44/mysql-connector-java-5.1.44-bin.jar" \
--driver="com.mysql.jdbc.Driver" \
--changeLogFile="/var/www/html/migrations/changelog.xml" \
--url="jdbc:mysql://$MYSQL_DB_HOST:$MYSQL_DB_PORT/$MYSQL_DB_DATABASE" \
--username="$MYSQL_DB_USER" \
--password="$MYSQL_DB_PASSWORD" \
migrate

#Hace el restore de la DB.
mysql --host=$MYSQL_DB_HOST --user=$MYSQL_DB_USER --password=$MYSQL_DB_PASSWORD --port=$MYSQL_DB_PORT $MYSQL_DB_DATABASE < /tmp/bug_restore.sql


supervisord -n
